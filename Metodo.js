import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { DatePicker } from 'antd';
import { Button } from 'antd';
import { Table, Icon, Divider } from 'antd';

const dataSource = [{
  key: '1',
  name: 'Mike',
  age: 32,
  address: '10 Downing Street'
}, {
  key: '2',
  name: 'John',
  age: 42,
  address: '10 Downing Street'
}, {
  key: '3',
  name: 'Juanito',
  age: 55,
  address: 'En su casa'
}
];

const columns = [{
  title: 'Name',
  dataIndex: 'name',
  key: 'name',
}, {
  title: 'Age',
  dataIndex: 'age',
  key: 'age',
}, {
  title: 'Address',
  dataIndex: 'address',
  key: 'address',
}];

function Data(){
  return(
    <fieldset>
        <legend>Datapicker</legend>
        <h1>Fiu fiawdefwu</h1>
        <hr /><br />
        <DatePicker />
        <p>Es inutil lo se</p>      
      </fieldset>
  )
}

function Boton(){
  return(
    <fieldset>   
      <legend>Botones</legend>
        <Button type="primary">Primary</Button>
        <Button>Default</Button>
        <Button type="dashed">Dashed</Button>
        <Button type="danger">Danger</Button>
    </fieldset>
  )
}

function Tabla(){
  return(
    <div>
    <fieldset>
    <legend>Tabla</legend>
    <Table dataSource={dataSource} columns={columns} />
    </fieldset>
    </div>
  )
}

function App() {
  return (    
    <div style={{ margin: 100 }}>
      <Data />
      <Boton />
      <Tabla />
           
    </div>
    
  );
}


ReactDOM.render(<App />, document.getElementById('root'));